﻿/*
    Занегин Алексей, группа csharp-2020-02, Домашнее задание 2
    Реализовано:
    + Есть описание класса и операторов на русском/английском языке.
    + Есть класс.
    + Есть конструктор.
    + Есть методы.
    + Есть операторы.
    - Есть перегруженные операторы.
    + Создан индексатор.
    + Создан метод расширения для класса.
    + Написана программа для тестирования/демонстрации класса. 
 */
using System;

namespace Otus_Lesson2_Operators
{
    /// <summary>
    /// Общее описание:
    /// Статический класс - SpellLab - реализует лабораторию волшебника, 
    /// имеет метод CastSomeSpells, реализующий демонстрацию класса Spell - магических заклинаний. 
    /// В SpellLab размещается объект класса SpellBook - книга заклинаний, 
    /// в SpellBook размещается массив объектов класса Spell - заклинаний.
    /// Класс SpellBook, помимо конструткора по умолчанию, имеет индексатор для обращения к объекту Spell по индексу.
    /// Класс Spell имеет свойства Название заклинания Name, Сторона магии Side (реализовано через перечисление) и Сила заклинания Power. 
    /// Свойство Power имеет логику, сила не может быть меньше 1 и выше 10 пунктов.
    /// Класс имеет конструкторы, методы увеличения PowerUp() и уменьшения PowerDown() силы, оператор соединения(+) двух заклинаний,
    /// перегруженный метод ToString(), и метод расширения GetSpellSysInfo для получения инфо о заклинании в служебных целях.
    /// </summary>
    public static class SpellLab
    {   
        public static void CastSomeSpells()
        {
            SpellBook book = new SpellBook();

            Console.WriteLine("***Origin states of spells***");
            book[0] = new Spell("Shlapitakhus Arrakis!", SpellSide.dark, 10);
            book[1] = new Spell("Bulmagaudas Zerkoin!", SpellSide.bright, 1);
            book[2] = new Spell();

            for (int i=0; i < 3; i++)
                Console.WriteLine(book[i].ToString());

            Console.WriteLine("***Some modifications of spells***");
            Console.WriteLine("decreasing power of spell:"); 
            book[0].PowerDown();
            Console.WriteLine(book[0].ToString());

            Console.WriteLine("increasing power of spell:");
            book[1].PowerUp();
            Console.WriteLine(book[1].ToString());

            Console.WriteLine("increasing power of spell:");
            book[2].PowerUp();
            Console.WriteLine(book[2].ToString());

            Console.WriteLine("mixing of two spells:");
            Spell mixedSpell = null;
            mixedSpell = book[0] + book[1];
            Console.WriteLine(mixedSpell.ToString());

            Console.WriteLine("spell's system info:");
            Console.WriteLine(book[2].GetSpellSysInfo());
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            SpellLab.CastSomeSpells();
            Console.ReadKey();
        }
    }

    public class SpellBook
    {
        Spell[] book;

        public SpellBook()
        {
            book = new Spell[3];
        }

         public Spell this[int index]
         {
            get { return book[index]; }
            set { book[index] = value; }
         }
    }
    

    public enum SpellSide
    {
        dark,
        bright
    }

    public class Spell
    {
        private readonly Random _rndSpell = new Random();
        private const int maxPower = 10;
        private const int minPower = 1;
        
        private int power;
        public string Name { get; set; }
        public SpellSide Side { get; set; }
        public int Power 
        {
            get { return power; }
            set 
            {
                if (value >= minPower && value <= maxPower)
                    power = value;
                else
                {
                    if (value > 0)
                    {
                        Console.WriteLine($"The power of spell {this.Name} cannot be higher than limit {maxPower}!");
                        power = maxPower;
                    }
                    else
                    {
                        Console.WriteLine($"The power of spell {this.Name} cannot be lower than limit {minPower}!");
                        power = minPower;
                    }
                }
            }
        }

        public Spell()
        {
            Name = "Unknown Random Spell";
            Side = (SpellSide)(_rndSpell.Next(0, 2));
            Power = _rndSpell.Next(minPower, maxPower+1);
        }

        public Spell(string name, SpellSide side, int power)
        {
            Name = name;
            Side = side;
            Power = power;
        }

        public void PowerUp()
        {
            this.Power++;
        }

        public void PowerDown() 
        {
            this.Power--;
        }

        public static Spell operator + (Spell a, Spell b)
        {
            a.Name = a.Name + " " + b.Name;
            if (a.Side != b.Side)
                a.Power = Math.Abs(a.Power - b.Power);
            else
                a.Power = Math.Abs(a.Power + b.Power); 
            
            return a;
        }

        public override string ToString()
        {
            string str;
            if (this is Spell)
                str = $"You've got spell {this.Name}, belongs to {this.Side} magic and has power {this.Power}";
            else
                str = $"Something wrong with your spell:(";
            return str+"\n";
        }
    }

    public static class SpellExtension
    { 
        public static string GetSpellSysInfo(this Spell someSpell)
        {
            return $"Name={someSpell.Name};Side={someSpell.Side};Power={someSpell.Power}";
        }
    }
}
