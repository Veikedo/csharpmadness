﻿using System;

namespace Otus_Lesson4_Interfaces
{
    public enum crownShapeEnum
    {
        sphere,
        cone
    }
    public enum treeTypeEnum
    { 
        leaf,
        pine
    }
    [Serializable]
    public class Tree
    {
        private readonly Random _rndHeight = new Random();
        private readonly Random _rndForEnum = new Random();
        private const int _maxHeight = 11;
        private const int _minHeight = 1;

        private int height;
        private crownShapeEnum crownShape;
        private treeTypeEnum treeType;

        public int Height 
        {
            get 
            {
                return height; 
            }
            set 
            {
                if (value >= _minHeight && value < _maxHeight)
                    height = value;
                else
                {
                    Console.WriteLine($"Unexcepted value for tree height: {value}, set to default height = {_minHeight}");
                    height = _minHeight;
                } 
            }
        }
        public crownShapeEnum CrownShape
        {
            get{return crownShape;}
            set
            {
                if ((int)value >= 0 && (int)value < 2)
                    crownShape = value;
                else
                { 
                    Console.WriteLine($"Unexcepted value for tree crown shape: {value}, set to default shape = sphere");
                    crownShape = crownShapeEnum.sphere;
                }
            }
        }
        public treeTypeEnum TreeType 
        { 
            get{return treeType;}
            set
            {
                if ((int)value >= 0 && (int)value < 2)
                    treeType = value;
                else
                {
                    Console.WriteLine($"Unexcepted value for tree type: {value}, set to default type = leaf");
                    treeType = treeTypeEnum.leaf;
                }
            } 
        }

        public Tree()
        {
            Height = _rndHeight.Next(_minHeight, _maxHeight);
            CrownShape = (crownShapeEnum)(_rndForEnum.Next(0, 2));
            TreeType = (treeTypeEnum)(_rndForEnum.Next(0, 2));
        }

        public Tree(int hgt, crownShapeEnum crownShp, treeTypeEnum treeTp)
        {
            Height = hgt;
            CrownShape = crownShp;
            TreeType = treeTp;
        }

        public override string ToString()
        {
            return $"{base.ToString()}: Height={Height}; CrownShape={CrownShape}; TreeType={TreeType}.";

        }
    }
}
