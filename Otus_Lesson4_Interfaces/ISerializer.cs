﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Otus_Lesson4_Interfaces
{
    /*
    ISerializer<T>:
    Имеет два метода:
    string Serialize<T>(T item);
    T Deserialize<T>(Stream stream);
    */
    public interface ISerializer<T>
    {
        string Serialize<T>(T item);
        T Deserialize<T>(Stream stream);
    }
}
