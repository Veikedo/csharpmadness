﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;
using System.Text;

namespace Otus_Lesson4_Interfaces
{
     /*
     OtusXmlSerializer<T> : ISerializer<T>
     */
    public class OtusXMLSerializer<T> : ISerializer<T>
    {
        private static readonly XmlWriterSettings xmlWriterSettings;
        public  static IExtendedXmlSerializer serializer;
        static OtusXMLSerializer()
        {
            xmlWriterSettings = new XmlWriterSettings { Indent = true };

            serializer = new ConfigurationContainer()
            .UseAutoFormatting()
            .UseOptimizedNamespaces()
            .EnableImplicitTyping(typeof(T[]))
            .Create();
        }

        public T Deserialize<T>(Stream stream)
        {
            return serializer.Deserialize<T>(stream);
        }

        public string Serialize<T>(T item)
        {
            return serializer.Serialize(xmlWriterSettings, item);
        }
    }
}
