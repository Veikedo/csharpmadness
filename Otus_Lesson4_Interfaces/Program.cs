﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

namespace Otus_Lesson4_Interfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            OtusXMLSerializer<Tree> serializer = new OtusXMLSerializer<Tree>();

            using
            (
                FileStream fileStream = 
                File.OpenRead(@"C:\Users\azanegin\source\repos\Otus_Lesson4_Interfaces\to_deserialize_forest.xml")
            )
            {
                OtusStreamReader<Tree> streamReader = new OtusStreamReader<Tree>(fileStream, serializer);

                foreach (Tree item in streamReader)
                {
                    Console.Write(item.ToString());
                }
            }
            Console.ReadKey();
        }
    }
}
