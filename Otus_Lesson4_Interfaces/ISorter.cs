﻿using System.Collections.Generic;

namespace Otus_Lesson4_Interfaces
{
    public interface ISorter<T>
    {
        IEnumerable<T> Sort<T>(IEnumerable<T> notSortedItems);
    }
}