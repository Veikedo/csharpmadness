﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace Otus_Lesson4_Interfaces
{
    /*
    OtusStreamReader<T> : IEnumerable<T>, IDisposable
    В конструкторе получаем Stream и ISerializer<T>.В методе GetEnumerator() вызываем 
    serializer.Deserialize<T[]>(Stream) и проходимся в цикле по массиву, возвращая элементы с помощью yield return.
    Да, можно было бы просто вернуть массив, но мы заодно научимся работать с yield.
    */
    public class OtusStreamReader<T> : IEnumerable<T>, IDisposable
    {
        public Stream stream;
        public ISerializer<T> serializer;

        public OtusStreamReader (Stream argStream, ISerializer<T> argSerializer)
        {
            stream = argStream;
            serializer = argSerializer;
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            /*
             * Length потока соотвествует количеству символов в файле
             * Если десериализовать из потока <T> объект, то получаем корректно, но только один объект Tree (первый в xml файле)
             * При этом Position после этого становится равным Length, т.е. достигаем завершения потока 
            */
            /*
            Console.WriteLine(stream.Length);
            Console.WriteLine(stream.Position);
            T tree = serializer.Deserialize<T>(stream);
            Console.WriteLine(stream.Position);
            yield return tree;
            */

            Console.WriteLine(stream.Length);
            Console.WriteLine(stream.Position);
            
            //возникает ошибка System.InvalidOperationException: "'Otus_Lesson4_Interfaces.Tree' is not of type Otus_Lesson4_Interfaces.Tree[]."
            T[] tStreamArr = serializer.Deserialize<T[]>(stream);
            Console.WriteLine(stream.Position);

            foreach (T tItem in tStreamArr)
            {
                yield return tItem; 
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        private IEnumerator GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Dispose()
        {
            Dispose();
        }
    }
}
